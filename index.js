const express = require('express');
const expressWS = require('express-ws');
const WebSocket = require('ws');
const path = require('path');
const app = express();
const appWS = expressWS(app);
const port = 3000;
const { spawn } = require('child_process');

var fs = require('fs');
var sprintf = require('sprintf-js').sprintf;

app.get('/', (request, response) => {
  console.log("root endpoint");
  response.send('Hello from Express!');;
})

app.use('/static', express.static(path.join(__dirname, 'static')))

videos = {
  vid_image: 'data/img/%05d.jpg',
  vid_mask: 'data/mask/%05d.jpg'
}

let rawdata = fs.readFileSync('data/bbox.json');
let bbox_data = JSON.parse(rawdata);

socket_client = null;

function update_clients(data) {
  if (!socket_client || socket_client.readyState !== WebSocket.OPEN) {
    return;
  }
  socket_client.send(data);
}

const readFilePromise = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (error, data) => {
      if (error) reject(error);
      resolve(data);
    });
  });
};

function load_image(videoId, frameId) {
  const path = sprintf(videos[videoId], frameId);
  readFilePromise(path).then(
    data => ({ "command": "image", "videoId": videoId, "frameId": frameId, "imageData": data.toString('base64') })
  ).catch(
    err => ({ "command": "error", "message": "error" + err })
  ).then(
    data => update_clients(JSON.stringify(data))
  )
}

function load_bbox(videoId, frameId) {
  console.log(`loading bbox for ${frameId}`);
  data = { "command": "bbox", "videoId": videoId, "frameId": frameId, "bboxData": bbox_data[frameId] };
  update_clients(JSON.stringify(data));
}

function process_command(msg_json) {
  console.log("Processing ", msg_json);
  if (msg_json.command == "loadImage") {
    return load_image(msg_json.videoId, msg_json.frameId);
  }
  if (msg_json.command == "loadBBox") {
    return load_bbox(msg_json.videoId, msg_json.frameId);
  }

}

app.ws('/', (ws, req) => {
  ws.on('message', function (msg) {
    try {
      msg_json = JSON.parse(msg);
      process_command(msg_json);
    } catch (error) {
      console.error(`Failed to process message ${msg}. Error ${error}. Continue...`);
    }
  });

  ws.on('error', function (err) {
    console.log('Found error: ' + err);
  });

  ws.on('close', function () {
    console.log('connection closed.');
  });

  socket_client = ws;
});


app.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }
  console.log(`server is listening on ${port}`)
})


process.on('SIGINT', function () {
  console.log("terminating...")
  
  process.exit();
});
