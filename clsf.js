const express = require('express');
const expressWS = require('express-ws');
const WebSocket = require('ws');
const path = require('path');
const app = express();
const appWS = expressWS(app);
const port = 3000;
const { spawn } = require('child_process');
var fs = require('fs');
var sprintf = require('sprintf-js').sprintf;
const argv = require('yargs').argv;
const parse = require('csv-parse/lib/sync')
const stringify = require('csv-stringify/lib/sync')
const path_module = require('path');

const readFilePromise = (path) => {
  return new Promise((resolve, reject) => {
    fs.readFile(path, (error, data) => {
      if (error) reject(error);
      resolve(data);
    });
  });
};

var df = null;
var pathDf = null;
var dfChanged = false;
function init(path) {
  pathDf = path;
  fileData = fs.readFileSync(pathDf); 
  df = parse(fileData, { columns: true });
  console.log(`loaded ${pathDf}, number of rows ${df.length}`);
  console.log(df);
}

function findBackupName(path) {
  var now = new Date();
  const parent = path_module.dirname(path);
  const fname = path_module.basename(path)
  const isoNow = now.toISOString().replace(/:/g,'');
  return path_module.join(parent, 'backup', fname + '.' + isoNow);
}

function saveDf() {
  if (pathDf === null || df === null ) {
    console.log("saveDf: no df loaded")
    return;
  } 
  if (!dfChanged) {
    // console.log("saveDf: no changes, saving df skipped...");
    return;
  }
  console.log(`saving df to ${pathDf}`)
  const backupName = findBackupName(pathDf);
  fs.mkdirSync(path_module.dirname(backupName), { recursive: true });
  console.log(backupName)
  fs.copyFileSync(pathDf, backupName);
  const data = stringify(df, 
    {
      header: true, 
      // columns: ['frame_number','property_c','property_e','img','mask']
    }
    );
  console.log('saving...')
  fs.writeFileSync(pathDf, data);
  dfChanged = false;
  console.log('...Done');
}

setInterval(saveDf, 5000);

function loadImage(socket_client, basePath, videoId, frameId) {
  console.log("loadImage: ", basePath, videoId, frameId);
  if (basePath[videoId] === undefined)  {
    socket_client.send(JSON.stringify({ "command": "error", "message": `loadImage: base path for ${videoId} is undefined`}));
    return;
  }
  if (df[frameId] === undefined)  {
    socket_client.send(JSON.stringify({ "command": "error", "message": `loadImage: no data for frame ${frameId} of ${videoId}`}));
    return;
  }
  const path = basePath[videoId] + df[frameId][videoId];
  readFilePromise(path).then(
    data => ({ 
      "command": "image", 
      "videoId": videoId, 
      "frameId": frameId, 
      "imageData": data.toString('base64'),
      "property_c": df[frameId].property_c,
      "property_e": df[frameId].property_e,
     })
  ).catch(
    err => {
      console.log(`loadImage: ${err}`);
      return { "command": "error", "message": "error " + err }
    }
  ).then(
    data => socket_client.send(JSON.stringify(data))
  )
}


function setData(socket_client, basePath, msg_json) {
  df[msg_json.frameId].property_c = msg_json.property_c;
  df[msg_json.frameId].property_e = msg_json.property_e;
  dfChanged = true;
  loadImage(socket_client, basePath, msg_json.videoId, msg_json.frameId);
}


function process_command(socket_client, msg_json) {
  console.log("command ", msg_json);
  if (msg_json.command == "loadImage") {
    return loadImage(socket_client, argv.base, msg_json.videoId, msg_json.frameId);
  }
  if (msg_json.command == "setData") {
    return setData(socket_client, argv.base, msg_json);
  }
  console.log("Unknown command: ", msg_json);
}
 
app.ws('/', (ws, req) => {
  ws.on('message', function (msg) {
    try {
      process_command(ws, JSON.parse(msg));
    } catch (error) {
      console.error(`ws.onmessage: Failed to process message ${msg}. Error ${error}. \n${error.stack} \nContinue...`);
    }
  });

  ws.on('error', function (err) {
    console.log('Found error: ' + err);
  });

  ws.on('close', function () {
    console.log('connection closed.');
  });
});


app.get('/', (request, response) => {
  console.log("root endpoint");
  // response.send('Hello from Express! ' + df);
  response.redirect('static/clsf.html')
})

app.use('/static', express.static(path.join(__dirname, 'static')))

console.log(argv);
init(argv.df);
console.log("starting the server...")
app.listen(port, (err) => {
    if (err) {
      return console.log('something bad happened', err)
    }
    console.log(`server is listening on ${port}`)
})


process.on('SIGINT', function () {
  console.log("terminating...")

  process.exit();
});
