angular.module('myApp', []).controller('clsfController', function ($scope) {
    $scope.connectionStatus = "";
    $scope.data = {};


    $scope.idxCur = 0;
    $scope.bboxInfo = {};
    $scope.property_c = -1;
    $scope.property_e = -1;
    $scope.mode = 'property_c';
    $scope.stepAfterChange = false;

    $scope.getSocketUrl = function () {
        return 'ws://' + window.location.host + '/';
    };

    var wsUri = $scope.getSocketUrl()
    var websocket = null;

    function sendToSocket(ws, data) {
        if (!ws || ws.readyState !== ws.OPEN) {
            console.log("Unable to send data. socket is not opened.");
            return;
        }
        ws.send(JSON.stringify(data));
    }

    function connect() {
        websocket = new WebSocket(wsUri);
        
        websocket.onopen = function (evt) {
            console.log("connection established to ", wsUri);
    
            $scope.$apply(function () {
                $scope.connectionStatus = $scope.connectionStatus + "connected to " + wsUri;
            });
    
            $scope.refreshFrame();
        };
    
        websocket.onerror = function (evt) {
            console.error('websocket error: ' + evt);
            $scope.connectionStatus = "error";
        };

        websocket.onclose = function(e) {
            $scope.connectionStatus = "closed";
            console.log('Socket is closed. Reconnect will be attempted in 1 second.', e.reason);
            setTimeout(function() {
              connect();
            }, 1000);
          };
    
        websocket.onmessage = function (evt) {
            var msgJson = JSON.parse(evt.data);
    
            $scope.$apply(function () {
                console.log("got message " + msgJson.command);
                if (msgJson.command == "error") {
                    $scope.connectionStatus = msgJson.message;
                    return;
                }
                if (msgJson.command == "image") {
                    if (msgJson.frameId == $scope.idxCur) {
                        $scope.data[msgJson.videoId] = 'data:image/*;base64,' + msgJson.imageData;
                        $scope.property_c = msgJson.property_c;
                        $scope.property_e = msgJson.property_e;
                    }
                    return;
                }
            });
        };
    }
    
    connect();

    $scope.imageData = function (videoId) {
        if ($scope.data[videoId]) 
            return $scope.data[videoId];
        return videoId;
    };

    $scope.refreshFrame = function () {
        ['img'].forEach(videoId => {
            sendToSocket(websocket, {
                "command": "loadImage",
                "videoId": videoId,
                "frameId": $scope.idxCur
            });
        });

    };



    $scope.navigate = function (event) {
        console.log(event)
        if ($scope.mode !== null) {
            const i = (event.key == '~') ? 0 : parseInt(event.key);
            if (!isNaN(i) ) {
                console.log(`sending data ${$scope.mode}, ${i}`)
                sendToSocket(websocket, {
                    "command": "setData",
                    "videoId": "img",
                    "frameId": $scope.idxCur,
                    "property_c": ($scope.mode == "property_c") ? i : $scope.property_c,
                    "property_e": ($scope.mode == "property_e") ? i : $scope.property_e
                });
                if ($scope.stepAfterChange) {
                    $scope.idxCur += 1;
                }
            }
        }
        
        if (event.key == "e") {
            $scope.mode = "property_e";
        } 
        if (event.key == "c") {    
            $scope.mode = "property_c";
        }
        if (event.key == "s") {    
            $scope.stepAfterChange = !$scope.stepAfterChange;
        }

        if (event.key == "Delete") {
            console.log(`sending data delete`)
            sendToSocket(websocket, {
                "command": "setData",
                "videoId": "img",
                "frameId": $scope.idxCur,
                "property_c": null,
                "property_e": null
            });
            if ($scope.stepAfterChange) {
                $scope.idxCur += 1;
            }
        } 
        
        if (event.key == "ArrowLeft") {
            $scope.idxCur -= 1;
        }
        if (event.key == "ArrowRight") {
            $scope.idxCur += 1;
        }
        if (event.key == "PageUp") {
            $scope.idxCur -= 30;
        }
        if (event.key == "PageDown") {
            $scope.idxCur += 30;
        }

        $scope.refreshFrame();

    }

});
