angular.module('myApp', []).controller('launcherCtrl', function ($scope) {
    $scope.connectionStatus = "";
    $scope.data = {
        'vid_image': "asdasd",
        'vid_mask': "jhkhk"
    };


    $scope.idxCur = 0;
    $scope.positions = {
        vid_mask: {
            current: 0,
            start: 0
        },
        vid_image: {
            current: 0,
            start: 0
        }
    }
    $scope.bboxInfo = {};


    $scope.getSocketUrl = function () {
        return 'ws://' + window.location.host + '/';
    };

    var wsUri = $scope.getSocketUrl()
    var websocket = new WebSocket(wsUri);

    websocket.onopen = function (evt) {
        console.log("connection established to ", wsUri);

        $scope.$apply(function () {
            $scope.connectionStatus = $scope.connectionStatus + "connected to " + wsUri;
        });

        $scope.refreshFrame();
    };

    websocket.onerror = function (evt) {
        console.error('websocket error: ' + evt);
        $scope.connectionStatus = "error";
    };

    websocket.onclose = function () {
        $scope.connectionStatus = "closed";
    };

    websocket.onmessage = function (evt) {
        var msgJson = JSON.parse(evt.data);

        $scope.$apply(function () {
            console.log("got message " + msgJson.command);
            if (msgJson.command == "error") {
                $scope.connectionStatus += msgJson.message;
                return;
            }
            if (msgJson.command == "image") {
                if (msgJson.frameId == $scope.positions[msgJson.videoId].current) {
                    $scope.data[msgJson.videoId] = 'data:image/jpeg;base64,' + msgJson.imageData;
                }
                return;
            }
            if (msgJson.command == "bbox") {
                if (msgJson.frameId == $scope.idxCur) {
                    $scope.bboxInfo = msgJson.bboxData;
                    $scope.drawBBox(1000);
                    console.log(JSON.stringify($scope.bboxInfo))
                }
                return;
            }
        });
    };

    $scope.loadImage = function (frameId) {
        console.log("requesting image " + frameId);
        websocket.send(JSON.stringify({
            "command": "load_image",
            "videoId": "vid_image",
            "frameId": frameId
        }));
        websocket.send(JSON.stringify({
            "command": "load_image",
            "videoId": "vid_mask",
            "frameId": frameId
        }));
    };

    $scope.imageData = function (videoId) {
        return $scope.data[videoId];
    };

    $scope.drawBBox = function (x) {
        var c = document.getElementById("myCanvas");
        var img = document.getElementById("layerImage")
        c.width = img.width;
        c.height = img.height;
        var ctx = c.getContext("2d");
        ctx.clearRect(0, 0, c.width, c.height);
        

        
        
        let bboxInfo = $scope.bboxInfo;
        console.log(`drawBBox ${bboxInfo}`)
        if ($scope.idxCur != bboxInfo.index) {
            return;
        }


        console.log(c.width / 1920, c.height / 1080);

        ctx.scale(c.width / 1920, c.height / 1080);

        
        // ctx.scale(0.3, 0.3);
        bboxInfo.labels.forEach(label => {
            let rect = label.box2d;
            ctx.lineWidth = "5";
            ctx.strokeStyle = "red";
            ctx.strokeRect(rect.x1, rect.y1, rect.x2 - rect.x1, rect.y2 - rect.y1);
            ctx.font = "15px Arial";
            ctx.fillText(`cat ${label.category} id ${label.attributes.tracking_id}`, rect.x1, rect.y1);
        });
        

        
        
    };

    $scope.refreshFrame = function () {
        ['vid_image', 'vid_mask'].forEach(videoId => {
            $scope.positions[videoId].current = $scope.positions[videoId].start + $scope.idxCur;
            websocket.send(JSON.stringify({
                "command": "loadImage",
                "videoId": videoId,
                "frameId": $scope.positions[videoId].current
            }));

            
        });

        websocket.send(JSON.stringify({
            "command": "loadBBox",
            "videoId": "vid",
            "frameId": $scope.idxCur
        }));

    };



    $scope.navigate = function (event) {
        // console.log(event)
        if (event.key == "ArrowLeft") {
            $scope.idxCur -= 1;
        }
        if (event.key == "ArrowRight") {
            $scope.idxCur += 1;
        }
        if (event.key == "PageUp") {
            $scope.idxCur -= 30;
        }
        if (event.key == "PageDown") {
            $scope.idxCur += 30;
        }
        $scope.refreshFrame();

    }

});
